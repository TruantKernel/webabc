# Web Application Building Components
An opinionated set of reusable components for JavaEE applications based on Domain Driven Design. A particular motivation for this library is the separation of Domain Objects from their respective JPA persistence models with the understanding that Domain Objects, as they relate to Domain Driven Design, should not be dependent on infrastructural concerns. 

This library is used to provide building blocks for its developer but has not been extensively tested in production. Please use with caution.

#### Configuration Components
###### PropertyProducer
A CDI producer providing injection of properties defined in a `/META-INF/config.properties` file. 

#### Domain Components (DDD)
###### DomainEntity
A class for DDD Entities that takes the type arguments `ID extends Serializable & Comparable<?>, EVENT extends DomainEvent`. The `ID` type argument specifies the class to serve as the identifier for a particular `DomainEntity`. This class requires the developer to implement a `public ID getId()` function. A `DomainEntity` includes a list of `DomainEvent`s that can be added to through the use of a `addEvent(event)` as operations are 	performed on the entity. The list of events can be retrieved via a `getDomainEvents()` function. This allows operations on the entity to be recorded and then retrieved for the purpose of propagating the events, such as for persistence.   
	
###### DomainEvent
A simple abstract event class that includes an event UUID identifier.

###### DomainEntityEvent
an abstract subclass of `DomainEvent` that includes the identifier of the subject Domain Object

###### DomainEventPublisher

an interface that takes the type argument `DOMAIN extends Object` where `DOMAIN` is the domain object the event is associated with. The interface specifies a single `publishEvents(DOMAIN instance)`. 

###### MappedDomainEventPublisher
an abstract subclass of `DomainEventPublisher` that includes an injected `EventBasedModelMapper`. Calling the `publishEvents` methods of this class results in `updatePersistenceModel` being passed the `DomainEntity` associated with the `EVENT` handled by the publisher. Through this mechanism changes on a domain model, tracked via events, can be persisted before publishing those events to the infrastructure layer. 
	
###### SimpleDomainId
A wrapper class to provide for an explicit identifier for a `DomainEntity`. This class take the type argument `ID extends Serializable & Comparable<? super ID>` where `ID` is a "Raw Identifier" that can be retrieved through the use of the `getRawIdentifier()` method. This class implements `Comparable<ID>` and `Serializable`

###### DomainUuid
An extension of the `SimpleDomainId` class that provides `UUID` as the type argument 

#### Mapper Components

###### ModelMapper
An interface that takes the type arguments `DOMAIN extends Object, PERSISTENCE extends PersistenceModel` where `DOMAIN` could be a `DomainEntity` or any other `Object`, and `PERSISTENCE` is a class that implements the `PersistenceModel` marker interface.

The `ModelMapper` interface specifies three methods: `persistenceModelToDomain(PERSISTENCE entity)` which takes the `PERSISTENCE` generic type argument and and returns the `DOMAIN` generic type argument; `domainToPersistenceModel(DOMAIN instance)` which takes `DOMAIN` and returns `PERSISTENCE`; and `updatePersistenceModel(DOMAIN instance, PERSISTENCE entity)` which takes `DOMAIN` and `PERSISTENCE` then should update `PERSISTENCE` based on the state of `DOMAIN`, returning `void`.

###### RequestScopedModelMapper
an abstract implementation of the `ModelMapper` interface that simply defines the mapper's `CDI` scope to be `RequestScoped`

###### EventBasedModelMapper
An abstract class that extends `RequestScopedModelMapper`. This class takes the type arguments `DOMAIN extends DomainEntity<?, EVENT>, PERSISTENCE extends PersistenceModel, EVENT extends DomainEvent, REQUEST extends EVENT` where `DOMAIN` is a `DomainEntity`, `PERSISTENCE` is a class that implements the `PersistenceModel` marker interface and serves as the persistence model for the `DomainEntity`, `EVENT` is the base `DomainEvent` for the `DomainEntity`, and `REQUEST` is the creational event that extends the base event for the `DomainEntity`. 

The purpose of this class is to provide a means to create and update `PersistenceModel`s based on the `DomainEvents` published by a `DomainEntity` and to create a `DomainEntity` based on the state of a `PersistenceModel`. By doing this infrastructural concerns such as persistence, and the model used for such persistence (such as a JPA Entity), can remain separate from the associated domain model. This helps a developer making use of this library adhere to the spirit of Domain Driven Design. 

The `EventBasedModelMapper` class specifies the `protected abstract` methods `createPersistenceModel(REQUEST event)`, and `createDomainModel(PERSISTENCE entity)`. A subclass is to override the `createPersistenceModel(REQUEST event)` method to define how the model mapper will create a `PersistenceModel` from the data contained in a creational event. A subclass is also to override the `createDomainModel(PERSISTENCE entity)` to define how the model mapper will instantiate a `DomainEntity` from the data contained in the `PersistenceModel`. 

In addition to the previous two methods, the `EventBasedModelMapper` class specifies the `protected abstract` methods `identifyAggregateRootCreationalEvent(EVENT event)` and `project(EVENT event)`. Due to type erasure, a subclass must override this method to test for the presence of the specific `EVENT` that serves as the creational event for the `DomainModel`. If `domainToPersistenceModel`, specified by the `ModelMapper` interface, is called but `identifyAggregateRootCreationalEvent(event)` fails to identify the creational event, then a `ModelMapperRuntimeException` is thrown. Finally a subclass is to override the `project(EVENT event)` method to define the actual mapping logic that translates updated data supplied by a `DomainEvent` into updates to be performed on a `PersistenceModel` received by calling `getTarget()` in the subclass.

The methods inherited by the `ModelMapper` interface are implemented in the `EventBasedModelMapper` class. The `domainToPersistenceModel(DOMAIN instance)` retrieves the domain events published to the list held by the `DomainEntity` and iterates across them to find the creational event, then calls `createPersistenceModel(REQUEST event)`, passing the creational event as a parameter. `persistenceModelToDomain(PERSISTENCE entity)` simply calls `createDomainModel(PERSISTENCE entity)`. `updatePersistenceModel(DOMAIN instance, PERSISTENCE entity)` retrieves the list of `EVENT`s from `DOMAIN`, sets `target` to the `PERSISTENCE` recieved as a parameter of the method, and iterates across the `EVENTS`, calling `this.project(event)` each iteration, and then finally setting `target` back to `null`. This implementation of the `ModelMapper` interface tracks mappings between `DOMAIN` and `PERSISTENCE` objects and because of that it's possible to call the overloaded `updatePersistenceModel(DOMAIN instance)` method that only requires the argument of type `DOMAIN`.

```java
public class AccountMapper 
extends EventBasedModelMapper<Account, AccountEntity, AccountEvent, AccountRegistered> 
implements AccountEventVisitor {
 {

	@Override
	protected Account createDomainModel(AccountEntity entity) {
		HashedCredentials basic = new HashedCredentials(entity.getUsername(), entity.getPassword());
		AccountCredentials credentials = new AccountCredentials(entity.getAccountEmail(), basic);
		return new Account(new AccountId(entity.getId()), entity.getIp(), credentials);
	}

	@Override
	protected void project(AccountEvent event) {
		event.accept(this);
	}

	@Override
	protected AccountEntity createPersistenceModel(AccountRegistered instance) {
	final UUID id = instance.getId()
	final EmailAddress email = instance.getId()
	final String username = instance.getId()
	final Password password = instance.getId()
	final IpAddress ip = instance.getId()
		return new AccountEntity(id, email, username, password, ip);
	}

	@Override
	public boolean identifyAggregateRootCreationalEvent(AccountEvent event) {
		return (event instanceof AccountRegistered);
	}

	// Visit methods omitted as they're examined later

}
```

###### ModelMapperRuntimeException

The exception thrown when `identifyAggregateRootCreationalEvent(event)` fails to identify the creational event during the process of instantiating a `PersistenceModel`. 

#### Pattern Components

###### Visitable
A simple interface used to implement the visitor pattern. A possible use of this interface is the optional utilization of "Double Dispatch" when implementing the `project(EVENT event)` method of the `EventBasedModelMapper` class. Below is an example of this.

```java
public class AccountMapper
extends EventBasedModelMapper<Account, AccountEntity, AccountEvent, AccountRegistered>
implements AccountEventVisitor {

	// unrelated but required methods omitted for brevity

	@Override
	protected void project(AccountEvent event) {
		event.accept(this);
	}

	@Override
	public void visit(AccountRegistered event) {
		return; // NOTHING TO DO HERE
	}

	@Override
	public void visit(AccountAssignedNewRole event) {
		this.getTarget().addAccountRole(event.getNewlyAssignedRole(), event.getAssigerId());
	}

}
```

```java
public interface AccountEventVisitor {

	void visit(AccountRegistered event);

	void visit(AccountAssignedNewRole accountAssignedNewRole);

}
```

#### Persistence Components

###### PersistenceModel
A marker interface to be applied to persistence models such as JPA Entity classes

#### Repository Components

###### Repository

An interface for Domain Driven Design Repositories. It takes the type arguments `DOMAIN extends Object, ID extends Serializable & Comparable<?>` where `DOMAIN` is the domain object that the repository provides a collection-like interface for, and where `ID` is the identifier used by that domain object.


###### MappedRepository
An abstract implementation of the Repository interface. The type argument `DOMAIN` for this class is further bounded by `DOMAIN extends DomainEntity<ID, ?>`. Additionally the class takes two additional type arguments: `PERSISTENCE extends PersistenceModel, MAPPER extends RequestScopedModelMapper<DOMAIN, PERSISTENCE>` where `PERSISTENCE` is a class that implements the `PersistenceModel` marker interface and serves as the persistence model associated with the domain object provided as the `DOMAIN` type argument. The type argument `MAPPER` is the `RequestScopedModelMapper` class that maps between the particular `DOMAIN` and `PERSISTENCE`. The `MappedRepository` provides a `CDI` injection point for the associated `MAPPER` class.

#### Servlet Components

###### ServletLifecycle

A utility class that provides static methods related to the life-cycle of a servlet.
