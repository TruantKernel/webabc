package usa.kerby.tk.webabc.persistence;

import java.io.Serializable;
import java.util.List;

import usa.kerby.tk.pageable.Pageable;

/**
 * @author Trevor Kerby
 * @since Nov 20, 2019
 */
public interface GenericDao<PERSISTENCE extends PersistenceModel, ID extends Serializable> {

	void joinTransaction();

	PERSISTENCE findById(ID id);

	// Optional<PERSISTENCE> findById(ID id, LockModeType lockModeType);

	List<PERSISTENCE> findAll(Pageable paging);

	List<PERSISTENCE> findAll();

	Long getCount();

	PERSISTENCE makePersistent(PERSISTENCE model) throws UniqueConstraintViolationException;

	void makeTransient(ID id);

	void makeTransient(PERSISTENCE model);

	void checkVersion(PERSISTENCE model, boolean forceUpdate);
}
