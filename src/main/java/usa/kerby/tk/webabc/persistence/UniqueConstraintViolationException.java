package usa.kerby.tk.webabc.persistence;

/**
 * @author Trevor Kerby
 * @since Mar 18, 2021
 */
public class UniqueConstraintViolationException extends Exception {
	private static final long serialVersionUID = 1L;

	String constraintName;

	public UniqueConstraintViolationException(String message, String constraintName) {
		super(message);
		this.constraintName = constraintName;
	}
	
	public String getConstraintName() {
		return this.constraintName;
	}
}
