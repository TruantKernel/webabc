package usa.kerby.tk.webabc.identity;

import java.util.UUID;

import com.github.f4b6a3.uuid.UuidCreator;
import com.github.f4b6a3.uuid.codec.BinaryCodec;
import com.github.f4b6a3.uuid.codec.UuidCodec;
import com.github.f4b6a3.uuid.codec.other.SlugCodec;

/**
 * @author Trevor Kerby
 * @since Apr 27, 2021
 */
public class UuidUtil {

	private UuidUtil() {

	}

	public static UUID generateTimeOrderedWithHash() {
		return UuidCreator.getTimeOrderedWithHash();
	}

	public static UUID generateTimeOrderedWithRandom() {
		return UuidCreator.getTimeOrderedWithRandom();
	}

	public static UUID generateTimeOrdered() {
		return UuidCreator.getTimeOrdered();
	}

	public static UUID generateRandom() {
		return UuidCreator.getRandomBased();
	}

	public static UUID generateTypedTimeOrderedWithHash(byte boundedContextId, short entityTypeId) {
		return TypedUuidCreator.getTimeOrderedWithHash(boundedContextId, entityTypeId);
	}

	public static UUID generateTypedTimeOrderedWithRandom(byte boundedContextId, short entityTypeId) {
		return TypedUuidCreator.getTimeOrderedWithRandom(boundedContextId, entityTypeId);
	}

	public static long getTimestamp(UUID id) {
		return com.github.f4b6a3.uuid.util.UuidUtil.getTimestamp(id);
	}
	
	public static long getNodeId(UUID id) {
		return com.github.f4b6a3.uuid.util.UuidUtil.getNodeIdentifier(id);
	}

	public static byte[] generateUuidBytes() {
		return UuidUtil.uuidToBytes(UuidCreator.getTimeOrdered());
	}

	public static byte[] generateRandomUuidBytes() {
		return UuidUtil.uuidToBytes(UuidCreator.getRandomBased());
	}

	public static UUID stringToUuid(String id) {
		return SlugCodec.INSTANCE.decode(id);
	}

	public static String uuidToString(UUID id) {
		return SlugCodec.INSTANCE.encode(id);
	}

	public static byte[] uuidToBytes(UUID id) {
		UuidCodec<byte[]> codec = new BinaryCodec();
		return codec.encode(id);
	}

	public static UUID bytesToUuid(byte[] uuidBytes) {
		UuidCodec<byte[]> codec = new BinaryCodec();
		return codec.decode(uuidBytes);
	}
}
