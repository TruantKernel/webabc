package usa.kerby.tk.webabc.identity;

import java.util.UUID;

import com.github.f4b6a3.uuid.UuidCreator;

/**
 * @author Trevor Kerby
 * @since Jun 11, 2021
 */
public class DomainUuid extends SimpleDomainId<UUID> {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public static <T extends DomainUuid> T fromExisting(UUID rawIdentifier) {
		// Default implementation returns a new instance of MyBaseClass
		return (T) new DomainUuid(rawIdentifier);
	}

	public DomainUuid(UUID rawIdentifier) {
		super(rawIdentifier);
	}

	protected DomainUuid() {
		this(GenerationStrategy.TIME_ORDERED_HASH);
	}

	protected DomainUuid(GenerationStrategy strategy) {
		this(generateId(strategy));
	}

	protected static UUID generateId(GenerationStrategy strategy) {
		switch (strategy) {
		case TIME_ORDERED_HASH:
			return UuidCreator.getTimeOrderedWithHash();
		case TIME_ORDERED_RANDOM:
			return UuidCreator.getTimeOrderedWithRandom();
		case RANDOM:
			return UuidCreator.getRandomBased();
		default:
			return UuidCreator.getTimeOrdered();
		}
	}

	public static enum GenerationStrategy {
		TIME_ORDERED_HASH, TIME_ORDERED_RANDOM, RANDOM
	}

}
