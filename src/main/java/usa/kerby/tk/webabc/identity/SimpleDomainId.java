package usa.kerby.tk.webabc.identity;

import java.io.Serializable;

/**
 * @author Trevor Kerby
 * @since May 16, 2021
 */
public class SimpleDomainId<ID extends Serializable & Comparable<? super ID>>
		implements Comparable<SimpleDomainId<ID>>, Serializable {
	private static final long serialVersionUID = 1L;
	protected final ID identifier;

	public SimpleDomainId(ID rawIdentifier) {
		this.identifier = rawIdentifier;
	}

	public ID getRawIdentifier() {
		return this.identifier;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (this == o) {
			return true;
		}
		if ((o instanceof SimpleDomainId) && (((SimpleDomainId<ID>) o).identifier.equals(this.identifier))) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return this.identifier.hashCode();
	}

	@Override
	public int compareTo(SimpleDomainId<ID> o) {
		return identifier.compareTo(o.identifier);
	}

	@Override
	public String toString() {
		return this.identifier.toString();
	}
	
	public boolean representedBy(ID rawIdentifier) {
		return this.identifier.equals(rawIdentifier);
	}

}
