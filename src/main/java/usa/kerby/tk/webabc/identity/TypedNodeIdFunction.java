package usa.kerby.tk.webabc.identity;

import com.github.f4b6a3.uuid.factory.function.NodeIdFunction;

public abstract class TypedNodeIdFunction implements NodeIdFunction {

	public TypedNodeIdFunction(byte boundedContextId, short entityTypeId) {
		validateArguements(boundedContextId, entityTypeId);
	}

	protected static long boundedContextIdToLong(byte boundedContextId) {
		long id = ((long) boundedContextId << 41) & 0x0000_FE00_0000_0000L;
		return id;
	}

	protected static long entityTypeIdToLong(short entityTypeId) {
		return ((long) entityTypeId << 32) & 0x0000_00FF_0000_0000L;
	}

	protected static long embedIds(long systemNodeId, byte boundedContextId, short entityTypeId) {
		return ((systemNodeId & 0x0000_0000_ffffffffL) | boundedContextIdToLong(boundedContextId)
				| entityTypeIdToLong(entityTypeId));
	}

	protected static void validateArguements(byte boundedContextId, short entityTypeId) {
		if (!isValidBoundedContextId(boundedContextId)) {
			throw new IllegalArgumentException("Invalid Bounded Context Id: " + boundedContextId);
		}
		if (!isValidEntityTypeId(entityTypeId)) {
			throw new IllegalArgumentException("Invalid Entity Type Id: " + entityTypeId);
		}
	}

	protected static boolean isValidBoundedContextId(byte boundedContextId) {
		return (boundedContextId >= 0);
	}

	protected static boolean isValidEntityTypeId(short entityTypeId) {
		return (entityTypeId >= 0 && entityTypeId <= 254);
	}

}
