package usa.kerby.tk.webabc.identity;

import com.github.f4b6a3.uuid.factory.function.NodeIdFunction;
import com.github.f4b6a3.uuid.util.MachineId;
import com.github.f4b6a3.uuid.util.internal.ByteUtil;

public class TypedHashNodeIdFunction extends TypedNodeIdFunction {

	private final long nodeIdentifier;

	public TypedHashNodeIdFunction(byte boundedContextId, short entityTypeId) {
		super(boundedContextId, entityTypeId);
		final byte[] hash = MachineId.getMachineHash();
		final long number = ByteUtil.toNumber(hash, 0, 4);
		final long id = embedIds(number, boundedContextId, entityTypeId);
		this.nodeIdentifier = NodeIdFunction.toMulticast(id);
	}

	@Override
	public long getAsLong() {
		return this.nodeIdentifier;
	}
}
