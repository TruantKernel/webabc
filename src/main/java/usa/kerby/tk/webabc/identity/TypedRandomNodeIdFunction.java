package usa.kerby.tk.webabc.identity;

import com.github.f4b6a3.uuid.factory.function.NodeIdFunction;
import com.github.f4b6a3.uuid.util.internal.RandomUtil;

public class TypedRandomNodeIdFunction extends TypedNodeIdFunction {

	private final long nodeIdentifier;

	public TypedRandomNodeIdFunction(byte boundedContextId, short entityTypeId) {
		super(boundedContextId, entityTypeId);
		final long number = RandomUtil.nextLong();
		final long id = embedIds(number, boundedContextId, entityTypeId);
		this.nodeIdentifier = NodeIdFunction.toMulticast(id);
	}

	@Override
	public long getAsLong() {
		return this.nodeIdentifier;
	}
}