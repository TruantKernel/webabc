package usa.kerby.tk.webabc.identity;

import java.util.UUID;

import com.github.f4b6a3.uuid.factory.rfc4122.TimeOrderedFactory;

public class TypedUuidCreator {

	public static UUID getTimeOrderedWithHash(byte boundedContextId, short entityId) {
		return TimeOrderedFactory.builder().withNodeIdFunction(new TypedHashNodeIdFunction(boundedContextId, entityId))
				.build().create();
	}

	public static UUID getTimeOrderedWithRandom(byte boundedContextId, short entityId) {
		return TimeOrderedFactory.builder()
				.withNodeIdFunction(new TypedRandomNodeIdFunction(boundedContextId, entityId)).build().create();
	}
}
