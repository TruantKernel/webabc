package usa.kerby.tk.webabc.identity;

import java.util.UUID;

public abstract class TypedDomainUuid extends DomainUuid {

	private static final long serialVersionUID = 1L;
	private byte expectedBoundedContextId;
	private short expectedEntityTypeId;

	public TypedDomainUuid(UUID rawIdentifier) {
		super(rawIdentifier);
	}

	protected TypedDomainUuid(byte boundedContextId, short entityTypeId, TypedGenerationStrategy strategy) {
		this(generateId(boundedContextId, entityTypeId, strategy));
		this.expectedBoundedContextId = boundedContextId;
		this.expectedEntityTypeId = entityTypeId;
	}

	public byte getExpectedBoundedContextId() {
		return this.expectedBoundedContextId;
	}

	public short getExpectedEntityTypeId() {
		return this.expectedEntityTypeId;
	}

	protected static UUID generateId(byte boundedContextId, short entityTypeId, TypedGenerationStrategy strategy) {
		switch (strategy) {
		case TIME_ORDERED_HASH:
			return UuidUtil.generateTypedTimeOrderedWithHash(boundedContextId, entityTypeId);
		case TIME_ORDERED_RANDOM:
			return UuidUtil.generateTypedTimeOrderedWithRandom(boundedContextId, entityTypeId);
		default:
			return UuidUtil.generateTypedTimeOrderedWithHash(boundedContextId, entityTypeId);
		}
	}

	public void validate() {
		final long nodeId = UuidUtil.getNodeId(identifier);

		final byte boundedContenxtId = extractBoundedContextId(nodeId);
		if (!isValidBoundedContextId(boundedContenxtId)) {
			throw new IllegalStateException("Bounded Context Id " + boundedContenxtId + " does not equal expected "
					+ getExpectedBoundedContextId());
		}

		final short entityTypeId = extractEntityTypeId(nodeId);
		if (!isValidEntityTypeId(entityTypeId)) {
			throw new IllegalStateException(
					"Entity Typed Id " + entityTypeId + " does not equal expected " + getExpectedEntityTypeId());
		}
	}

	public boolean isValid() {
		final long nodeId = UuidUtil.getNodeId(identifier);
		return (isValidBoundedContextId(nodeId) && isValidEntityTypeId(nodeId));
	}

	private boolean isValidBoundedContextId(long nodeId) {
		return isValidBoundedContextId(extractBoundedContextId(nodeId));
	}

	private boolean isValidBoundedContextId(byte boundedContextId) {
		return (boundedContextId == getExpectedBoundedContextId());
	}

	private boolean isValidEntityTypeId(long nodeId) {
		return isValidEntityTypeId(extractEntityTypeId(nodeId));
	}

	private boolean isValidEntityTypeId(short entityTypeId) {
		return (entityTypeId == getExpectedEntityTypeId());
	}

	protected static byte extractBoundedContextId(UUID id) {
		return extractBoundedContextId(UuidUtil.getNodeId(id));
	}

	protected static byte extractBoundedContextId(long typedNodeId) {
		return (byte) ((typedNodeId >>> 41) & 0x7FL);
	}

	protected static short extractEntityTypeId(UUID id) {
		return extractEntityTypeId(UuidUtil.getNodeId(id));
	}

	protected static short extractEntityTypeId(long typedNodeId) {
		return (short) ((typedNodeId >>> 32) & 0xFFL);
	}

	protected static int extractSystemNodeId(UUID id) {
		return extractSystemNodeId(UuidUtil.getNodeId(id));
	}

	protected static int extractSystemNodeId(long typedNodeId) {
		return (int) (typedNodeId & 0xFFFFFFFFL);
	}

	@Override
	public boolean equals(Object o) {
		return super.equals(o);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public static enum TypedGenerationStrategy {
		TIME_ORDERED_HASH, TIME_ORDERED_RANDOM
	}

}
