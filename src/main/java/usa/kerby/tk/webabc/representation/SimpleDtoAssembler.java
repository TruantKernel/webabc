package usa.kerby.tk.webabc.representation;

import java.util.List;
import java.util.stream.Collectors;

import usa.kerby.tk.jhateoas.DataTransferObject;

public abstract class SimpleDtoAssembler<DTO extends DataTransferObject, DOMAIN>
		implements DtoAssembler<DTO, DOMAIN> {

	protected abstract DTO buildDataTransferObject(DOMAIN obj);

	public DTO assemble(DOMAIN obj) {
		if (obj == null) {
			return null;
		}
		return this.buildDataTransferObject(obj);
	}
	
	public List<DTO> assemble(List<DOMAIN> list) {
		return list.stream()
				.filter(instance -> instance != null)
				.map(instance -> this.buildDataTransferObject(instance))
				.collect(Collectors.toList());
	}
}
