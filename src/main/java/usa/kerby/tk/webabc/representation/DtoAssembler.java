package usa.kerby.tk.webabc.representation;

import java.util.List;

import usa.kerby.tk.jhateoas.DataTransferObject;

public interface DtoAssembler<DTO extends DataTransferObject, DOMAIN> {
	
	public DTO assemble(DOMAIN obj);

	public List<DTO> assemble(List<DOMAIN> list);

}
