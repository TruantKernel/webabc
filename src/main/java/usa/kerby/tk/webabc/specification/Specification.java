package usa.kerby.tk.webabc.specification;

public interface Specification<T> {

	public boolean isSatisfiedBy(T candidate);

}
