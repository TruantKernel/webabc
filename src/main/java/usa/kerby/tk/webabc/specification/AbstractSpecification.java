package usa.kerby.tk.webabc.specification;

public abstract class AbstractSpecification<T> implements Specification<T> {

	public abstract boolean isSatisfiedBy(T t);

	public AbstractSpecification<T> or(Specification<T> compositeNode) {
		return new DisjunctionSpecification<T>(this, compositeNode);
	}

	public AbstractSpecification<T> and(Specification<T> compositeNode) {
		return new ConjunctionSpecification<T>(this, compositeNode);
	}

	public AbstractSpecification<T> not() {
		return new NegationSpecification<T>(this);
	}

}
