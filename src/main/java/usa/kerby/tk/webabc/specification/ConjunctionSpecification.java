package usa.kerby.tk.webabc.specification;

import java.util.HashSet;
import java.util.Set;

public class ConjunctionSpecification<T> extends AbstractSpecification<T> {

	private Set<Specification<T>> composite = new HashSet<Specification<T>>();

	public ConjunctionSpecification(Specification<T> a, Specification<T> b) {
		composite.add(a);
		composite.add(b);
	}

	public boolean isSatisfiedBy(T t) {
		for (Specification<T> s : composite) {
			if (!s.isSatisfiedBy(t)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public AbstractSpecification<T> and(Specification<T> s) {
		composite.add(s);
		return this;
	}
}