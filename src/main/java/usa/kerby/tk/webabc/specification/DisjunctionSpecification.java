package usa.kerby.tk.webabc.specification;

import java.util.HashSet;
import java.util.Set;

public class DisjunctionSpecification<T> extends AbstractSpecification<T> {

	private Set<Specification<T>> composite = new HashSet<Specification<T>>();

	public DisjunctionSpecification(Specification<T> a, Specification<T> b) {
		composite.add(a);
		composite.add(b);
	}

	public boolean isSatisfiedBy(T t) {
		for (Specification<T> s : composite) {
			if (s.isSatisfiedBy(t)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public AbstractSpecification<T> or(Specification<T> s) {
		composite.add(s);
		return this;
	}
}
