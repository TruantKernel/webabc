package usa.kerby.tk.webabc.specification;

public class NegationSpecification<T> extends AbstractSpecification<T> {

	private Specification<T> spec;

	public NegationSpecification(Specification<T> s) {
		this.spec = s;
	}

	@Override
	public boolean isSatisfiedBy(T t) {
		return !spec.isSatisfiedBy(t);
	}

}
