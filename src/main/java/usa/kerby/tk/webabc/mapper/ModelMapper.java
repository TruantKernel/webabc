package usa.kerby.tk.webabc.mapper;

import usa.kerby.tk.webabc.persistence.PersistenceModel;

/**
 * @author Trevor Kerby
 * @since May 3, 2021
 */
public interface ModelMapper<DOMAIN extends Object, PERSISTENCE extends PersistenceModel> {

	public DOMAIN persistenceModelToDomain(PERSISTENCE entity);

	public PERSISTENCE domainToPersistenceModel(DOMAIN instance);

	public void updatePersistenceModel(DOMAIN instance, PERSISTENCE entity);

	void removeMapping(DOMAIN instance);

}
