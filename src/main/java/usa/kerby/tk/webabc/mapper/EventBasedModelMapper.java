package usa.kerby.tk.webabc.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import usa.kerby.tk.webabc.domain.DomainEntity;
import usa.kerby.tk.webabc.domain.DomainEvent;
import usa.kerby.tk.webabc.persistence.PersistenceModel;

/**
 * @author Trevor Kerby
 * @since May 3, 2021
 */
public abstract class EventBasedModelMapper<DOMAIN extends DomainEntity<?, EVENT>,
											PERSISTENCE extends PersistenceModel,
											EVENT extends DomainEvent,
											REQUEST extends EVENT>
		extends RequestScopedModelMapper<DOMAIN, PERSISTENCE> {

	private Map<DOMAIN, PERSISTENCE> mapping = new HashMap<>();

	private PERSISTENCE target;

	protected abstract PERSISTENCE createPersistenceModel(REQUEST event);

	protected abstract DOMAIN createDomainModel(PERSISTENCE entity);

	@Override
	public DOMAIN persistenceModelToDomain(PERSISTENCE entity) {
		if (entity == null ) {
			return null;
		}
		DOMAIN instance = createDomainModel(entity);
		this.mapping.put(instance, entity);
		return instance;
	}

	public void visit(REQUEST event) {
		return; // NOTHING TO DO HERE
	}

	public abstract boolean identifyAggregateRootCreationalEvent(EVENT event);

	@SuppressWarnings("unchecked")
	@Override
	public PERSISTENCE domainToPersistenceModel(DOMAIN instance) {
		PERSISTENCE entity = this.mapping.get(instance);
		if (entity != null) {
			return entity;
		}
		REQUEST creationalEvent = null;
		for (EVENT event : instance.getDomainEvents()) {
			if (this.identifyAggregateRootCreationalEvent(event)) {
				creationalEvent = (REQUEST) event;
				break;
			}
		}
		if (creationalEvent == null) {
			new ModelMapperRuntimeException("domainToPersistenceModel was called for "
					+ instance.getClass().getSimpleName() + ", but no Creational Event was found");
		}
		entity = createPersistenceModel(creationalEvent);
		this.mapping.put(instance, entity);
		return entity;
	}

	protected abstract void project(EVENT event);

	public void updatePersistenceModel(DOMAIN instance) {
		this.updatePersistenceModel(instance, this.mapping.get(instance));
	}

	@Override
	public void updatePersistenceModel(DOMAIN instance, PERSISTENCE entity) {
		List<EVENT> events = (List<EVENT>) instance.getDomainEvents();
		if (events == null) {
			return;
		}
		this.target = entity;
		for (EVENT event : events) {
			this.project(event);
		}
		this.target = null;
	}

	public DOMAIN getDomainObjectById(Object id) {
		for (DOMAIN domainObject : mapping.keySet()) {
			if (domainObject.getId().equals(id)) {
				return domainObject;
			}
		}
		return null;
	}
	
	@Override
	public void removeMapping(DOMAIN instance) {
		this.mapping.remove(instance);
	}

	protected PERSISTENCE getTarget() {
		return this.target;
	}

}
