package usa.kerby.tk.webabc.mapper;

import jakarta.enterprise.context.RequestScoped;
import usa.kerby.tk.webabc.persistence.PersistenceModel;

@RequestScoped
public abstract class RequestScopedModelMapper<DOMAIN extends Object, PERSISTENCE extends PersistenceModel>
		implements ModelMapper<DOMAIN, PERSISTENCE> {

}
