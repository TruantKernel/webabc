package usa.kerby.tk.webabc.mapper;

/**
 * @author Trevor Kerby
 * @since Jun 29, 2021
 */
public class ModelMapperRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ModelMapperRuntimeException(String message) {
		super(message);
	}

}
