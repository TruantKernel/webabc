package usa.kerby.tk.webabc.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.InjectionPoint;

@ApplicationScoped
public class PropertyProducer {

	private Properties properties;

	@Property
	@Produces
	public String produceString(final InjectionPoint ip) {
		return this.properties.getProperty(getKey(ip));
	}

	@Property
	@Produces
	public int produceInt(final InjectionPoint ip) {
		return Integer.valueOf(this.properties.getProperty(getKey(ip)));
	}

	@Property
	@Produces
	public boolean produceBoolean(final InjectionPoint ip) {
		return Boolean.valueOf(this.properties.getProperty(getKey(ip)));
	}

	private String getKey(final InjectionPoint ip) {
		if (ip.getAnnotated().isAnnotationPresent(Property.class)
				&& !ip.getAnnotated().getAnnotation(Property.class).value().isEmpty()) {
			return ip.getAnnotated().getAnnotation(Property.class).value();
		} else {
			return ip.getMember().getName();
		}

	}

	@PostConstruct
	public void init() {
		this.properties = new Properties();
		final String path = "/META-INF/config.properties";
		final InputStream stream = PropertyProducer.class.getResourceAsStream(path);
		if (stream == null) {
			throw new RuntimeException("Cannot find properties file at path: " + path);
		}
		try {
			this.properties.load(stream);
		} catch (final IOException e) {
			throw new RuntimeException("Configuration could not be loaded!");
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
