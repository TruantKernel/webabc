package usa.kerby.tk.webabc.application;

public class FeatureNotActiveException extends Exception {
	private static final long serialVersionUID = 1L;
	private static final String UNSPECIFIED = "Unspecified Feature";

	public final String feature;

	public FeatureNotActiveException() {
		this.feature = UNSPECIFIED;
	}

	public FeatureNotActiveException(String feature) {
		this.feature = feature;
	}

	public String getFeatureName() {
		return this.feature;
	}

}
