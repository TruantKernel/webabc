package usa.kerby.tk.webabc.pattern;

/**
 * @author Trevor Kerby
 * @since Jun 29, 2021
 */
public interface Visitable<V> {
	void accept(V visitor);
}
