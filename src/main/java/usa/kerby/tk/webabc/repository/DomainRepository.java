package usa.kerby.tk.webabc.repository;

import java.io.Serializable;

import usa.kerby.tk.webabc.domain.DomainEntity;
import usa.kerby.tk.webabc.identity.SimpleDomainId;
import usa.kerby.tk.webabc.mapper.RequestScopedModelMapper;
import usa.kerby.tk.webabc.persistence.GenericDao;
import usa.kerby.tk.webabc.persistence.PersistenceModel;

public class DomainRepository<	DOMAIN extends DomainEntity<ID, ?>, PERSISTENCE extends PersistenceModel,
								MAPPER extends RequestScopedModelMapper<DOMAIN, PERSISTENCE>,
								ID extends SimpleDomainId<KEY>,
								KEY extends Serializable & Comparable<KEY>,
								DAO extends GenericDao<PERSISTENCE, KEY>>
		extends DaoMappedRepository<DOMAIN, PERSISTENCE, MAPPER, ID, KEY, DAO> {

	@Override
	protected KEY convertIdToPrimaryKey(ID id) {
		return id.getRawIdentifier();
	}

}
