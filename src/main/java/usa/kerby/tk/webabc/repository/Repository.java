package usa.kerby.tk.webabc.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import usa.kerby.tk.pageable.Pageable;

/**
 * @author Trevor Kerby
 * @since May 3, 2021
 */
public interface Repository<DOMAIN extends Object, ID extends Serializable & Comparable<?>> {

	public Optional<DOMAIN> find(ID id);

	public List<DOMAIN> findAll(Pageable paging);

	public void add(DOMAIN instance) throws Exception;

	public void remove(DOMAIN instance);

	public long size();
}
