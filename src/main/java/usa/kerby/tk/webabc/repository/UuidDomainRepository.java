package usa.kerby.tk.webabc.repository;

import java.util.UUID;

import usa.kerby.tk.webabc.domain.DomainEntity;
import usa.kerby.tk.webabc.identity.DomainUuid;
import usa.kerby.tk.webabc.mapper.RequestScopedModelMapper;
import usa.kerby.tk.webabc.persistence.GenericDao;
import usa.kerby.tk.webabc.persistence.PersistenceModel;

public class UuidDomainRepository<	DOMAIN extends DomainEntity<ID, ?>, PERSISTENCE extends PersistenceModel,
									MAPPER extends RequestScopedModelMapper<DOMAIN, PERSISTENCE>,
									ID extends DomainUuid,
									DAO extends GenericDao<PERSISTENCE, UUID>>
		extends DomainRepository<DOMAIN, PERSISTENCE, MAPPER, ID, UUID, DAO> {

}
