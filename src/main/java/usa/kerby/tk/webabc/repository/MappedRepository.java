package usa.kerby.tk.webabc.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import usa.kerby.tk.webabc.domain.DomainEntity;
import usa.kerby.tk.webabc.mapper.RequestScopedModelMapper;
import usa.kerby.tk.webabc.persistence.PersistenceModel;

/**
 * @author Trevor Kerby
 * @since May 3, 2021
 */

@ApplicationScoped
public abstract class MappedRepository<	DOMAIN extends DomainEntity<ID, ?>, PERSISTENCE extends PersistenceModel,
										MAPPER extends RequestScopedModelMapper<DOMAIN, PERSISTENCE>,
										ID extends Serializable & Comparable<?>>
		implements Repository<DOMAIN, ID> {

	@Inject
	private MAPPER mapper;

	protected PERSISTENCE mapToPersistenceModel(DOMAIN instance) {
		return this.mapper.domainToPersistenceModel(instance);
	}

	protected Optional<DOMAIN> mapToDomainModel(PERSISTENCE entity) {
		return Optional.ofNullable(entity).map(result -> this.mapper.persistenceModelToDomain(result));
	}

	protected List<DOMAIN> mapToDomainModelCollection(List<PERSISTENCE> entityCollection) {
		return entityCollection.stream()
				.map(persistenceModel -> this.mapper.persistenceModelToDomain(persistenceModel))
				.collect(Collectors.toList());
	}

	protected void removeMapping(DOMAIN instance) {
		mapper.removeMapping(instance);
	}

}
