package usa.kerby.tk.webabc.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import usa.kerby.tk.pageable.Pageable;
import usa.kerby.tk.webabc.domain.DomainEntity;
import usa.kerby.tk.webabc.mapper.RequestScopedModelMapper;
import usa.kerby.tk.webabc.persistence.GenericDao;
import usa.kerby.tk.webabc.persistence.PersistenceModel;
import usa.kerby.tk.webabc.persistence.UniqueConstraintViolationException;

public abstract class DaoMappedRepository<	DOMAIN extends DomainEntity<ID, ?>, PERSISTENCE extends PersistenceModel,
											MAPPER extends RequestScopedModelMapper<DOMAIN, PERSISTENCE>,
											ID extends Serializable & Comparable<?>,
											KEY extends Serializable,
											DAO extends GenericDao<PERSISTENCE, KEY>>
		extends MappedRepository<DOMAIN, PERSISTENCE, MAPPER, ID> {

	@Inject
	protected DAO dao;

	protected abstract KEY convertIdToPrimaryKey(ID id);

	@Override
	public Optional<DOMAIN> find(ID id) {
		final KEY primaryKey = this.convertIdToPrimaryKey(id);
		final PERSISTENCE entity = this.dao.findById(primaryKey);
		if (entity == null) {
			return Optional.empty();
		}
		return this.mapToDomainModel(entity);
	}

	@Override
	public List<DOMAIN> findAll(Pageable paging) {
		final List<PERSISTENCE> persistenceEntities = this.dao.findAll(paging);
		return this.mapToDomainModelCollection(persistenceEntities);
	}

	@Override
	public void add(DOMAIN instance) throws Exception {
		try {
			this.save(instance);
		} catch (UniqueConstraintViolationException e) {
			throw e;
		}
	}

	protected void save(DOMAIN instance) throws UniqueConstraintViolationException {
		PERSISTENCE entity = this.mapToPersistenceModel(instance);
		this.dao.makePersistent(entity);
	}

	@Override
	public void remove(DOMAIN instance) {
		KEY primaryKey = this.convertIdToPrimaryKey(instance.getId());
		instance.addRemovalEvent();
		this.dao.makeTransient(primaryKey);
		this.removeMapping(instance);
	}

	@Override
	public long size() {
		return this.dao.getCount();
	}

}
