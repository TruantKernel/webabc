package usa.kerby.tk.webabc.domain;

import java.io.Serializable;

public abstract class DomainEntityEvent<ID extends Serializable & Comparable<?>> extends DomainEvent {
	private static final long serialVersionUID = 1L;
	private final ID subjectId;

	public DomainEntityEvent(ID subjectId) {
		this.subjectId = subjectId;
	}

	public ID getSubjectId() {
		return this.subjectId;
	}
}
