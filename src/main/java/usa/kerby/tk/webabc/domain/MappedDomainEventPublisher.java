package usa.kerby.tk.webabc.domain;

import java.util.List;

import jakarta.inject.Inject;
import usa.kerby.tk.webabc.mapper.EventBasedModelMapper;
import usa.kerby.tk.webabc.persistence.PersistenceModel;

public abstract class MappedDomainEventPublisher<	DOMAIN extends DomainEntity<?, EVENT>,
													PERSISTENCE extends PersistenceModel,
													EVENT extends DomainEntityEvent<?>,
													MAPPER extends EventBasedModelMapper<DOMAIN, PERSISTENCE, EVENT,
															? extends EVENT>>
		implements DomainEventPublisher<DOMAIN> {

	@Inject
	protected MAPPER mapper;

	public void publishEvents(List<EVENT> list) {
		if (!domainEventsPresent(list)) {
			return;
		}
		Object id = list.get(0).getSubjectId();
		DOMAIN instance = this.mapper.getDomainObjectById(id);
		if (instance != null) {
			mapper.updatePersistenceModel(instance);
		}
		this.propogate(list);
	}

	@Override
	public void publishEvents(DOMAIN instance) {
		mapper.updatePersistenceModel(instance);
		List<EVENT> list = instance.getDomainEvents();
		if (!domainEventsPresent(list)) {
			return;
		}
		this.propogate(list);
	}

	private boolean domainEventsPresent(List<EVENT> list) {
		return (list != null && !list.isEmpty());
	}

	protected abstract void propogate(List<EVENT> list);

}
