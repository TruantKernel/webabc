package usa.kerby.tk.webabc.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Trevor Kerby
 * @since Jun 26, 2021
 */
public abstract class DomainEntity<ID extends Serializable & Comparable<?>, EVENT extends DomainEvent> {

	private List<EVENT> domainEvents;

	public abstract ID getId();
	
	public EVENT initializeRemovalEvent() {
		return null;
	}
	
	public void addRemovalEvent() {
		this.addEvent(this.initializeRemovalEvent());
	}

	public void addEvent(EVENT event) {
		if (event == null) {
			return;
		}
		if (domainEvents == null) {
			this.domainEvents = new ArrayList<EVENT>();
		}
		domainEvents.add(event);
	}

	public List<EVENT> getDomainEvents() {
		return this.domainEvents;
	}
}
