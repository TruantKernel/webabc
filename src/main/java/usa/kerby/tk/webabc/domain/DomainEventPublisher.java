package usa.kerby.tk.webabc.domain;

public interface DomainEventPublisher<DOMAIN extends Object> {
 
	public void publishEvents(DOMAIN instance);
}
