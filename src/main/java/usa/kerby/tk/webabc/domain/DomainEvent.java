package usa.kerby.tk.webabc.domain;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author Trevor Kerby
 * @since Jun 26, 2021
 */
public abstract class DomainEvent implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final UUID eventId = UUID.randomUUID();

	public UUID getEventId() {
		return eventId;
	}
}
