package usa.kerby.tk.webabc.servlet;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

public class ServletLifecycle {

	public static void deregisterDrivers() {
		Enumeration<Driver> drivers = DriverManager.getDrivers();
		while (drivers.hasMoreElements()) {
			Driver driver = drivers.nextElement();
			try {
				DriverManager.deregisterDriver(driver);
				System.out.println(String.format("deregistering driver: %s", driver));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
