package usa.kerby.tk.webabc.identity;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import usa.kerby.tk.webabc.identity.TypedDomainUuid.TypedGenerationStrategy;

public class DomainUuidCreatorTest {

	private class ConcreteTypedDomainId extends TypedDomainUuid {
		private static final long serialVersionUID = 1L;

		protected ConcreteTypedDomainId(byte boundedContextId, short entityTypeId, TypedGenerationStrategy strategy) {
			super(boundedContextId, entityTypeId, strategy);
		}
	}

	@Test
	public void testHashedTypedDomainIdNumericArgumentLowerBounds() {
		ConcreteTypedDomainId id = new ConcreteTypedDomainId((byte) 0, (short) 0,
				TypedGenerationStrategy.TIME_ORDERED_HASH);
		id.validate();
	}

	@Test
	public void testHashedTypedDomainIdNumericArgumentUpperBounds() {
		ConcreteTypedDomainId id = new ConcreteTypedDomainId((byte) 127, (short) 254,
				TypedGenerationStrategy.TIME_ORDERED_HASH);
		id.validate();
	}

	@Test
	public void testRandomTypedDomainIdNumericArgumentLowerBounds() {
		ConcreteTypedDomainId id = new ConcreteTypedDomainId((byte) 0, (short) 0,
				TypedGenerationStrategy.TIME_ORDERED_RANDOM);
		id.validate();
	}

	@Test
	public void testRandomTypedDomainIdNumericArgumentUpperBounds() {
		ConcreteTypedDomainId id = new ConcreteTypedDomainId((byte) 127, (short) 254,
				TypedGenerationStrategy.TIME_ORDERED_RANDOM);
		id.validate();
	}

	@Test
	public void testInvalidRandomTypedDomainIdNumericArgumentUpperBounds() {
		assertThrows(IllegalArgumentException.class, new Executable() {

			@Override
			public void execute() throws Throwable {
				new ConcreteTypedDomainId((byte) 129, (short) 254, TypedGenerationStrategy.TIME_ORDERED_RANDOM);
			}

		});

	}
}
